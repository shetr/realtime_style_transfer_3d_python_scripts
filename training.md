# Training scripts

## by_eye_custom_fov90

### 01c reduced - input

```bash
python train.py --config "_custom_config/reduced.yaml" --data_root "training/by_eye_custom_fov90/01c-r-i_train" --log_interval 200 --log_folder logs_reference_P
```

### 01c reduced - input, normal

```bash
python train.py --config "_custom_config/reduced_normals.yaml" --data_root "training/by_eye_custom_fov90/01c-r-in_train" --log_interval 200 --log_folder logs_reference_P
```

### 01c reduced - input, light

```bash
python train.py --config "_custom_config/reduced_light.yaml" --data_root "training/by_eye_custom_fov90/01c-r-il_train" --log_interval 200 --log_folder logs_reference_P
```

### 01c reduced - input, disc

```bash
python train.py --config "_custom_config/reduced_disc.yaml" --data_root "training/by_eye_custom_fov90/01c-r-id_train" --log_interval 200 --log_folder logs_reference_P
```

### 01c reduced - disc

```bash
python train.py --config "_custom_config/reduced.yaml" --data_root "training/by_eye_custom_fov90/01c-r-d_train" --log_interval 200 --log_folder logs_reference_P
```

### 01c default - input

```bash
python train.py --config "_custom_config/default.yaml" --data_root "training/by_eye_custom_fov90/01c-d-i_train" --log_interval 200 --log_folder logs_reference_P
```

### 01c default - input, normal

```bash
python train.py --config "_custom_config/default_normals.yaml" --data_root "training/by_eye_custom_fov90/01c-d-in_train" --log_interval 200 --log_folder logs_reference_P
```

### 01c default - input, light

```bash
python train.py --config "_custom_config/default_light.yaml" --data_root "training/by_eye_custom_fov90/01c-d-il_train" --log_interval 200 --log_folder logs_reference_P
```

### 01c default - input, disc

```bash
python train.py --config "_custom_config/default_disc.yaml" --data_root "training/by_eye_custom_fov90/01c-d-id_train" --log_interval 200 --log_folder logs_reference_P
```

### 01c default - disc

```bash
python train.py --config "_custom_config/default.yaml" --data_root "training/by_eye_custom_fov90/01c-d-d_train" --log_interval 200 --log_folder logs_reference_P
```

### 01c reduced - input, gauss_custom1

```bash
python train.py --config "_custom_config/reduced_disc.yaml" --data_root "training/by_eye_custom_fov90/01c-r-ig1_train" --log_interval 200 --log_folder logs_reference_P
```

### 01c reduced2 - input, gauss_custom1

```bash
python train.py --config "_custom_config/reduced2_disc.yaml" --data_root "training/by_eye_custom_fov90/01c-r2-ig1_train" --log_interval 200 --log_folder logs_reference_P
```

### 01c reduced3 - input, gauss_custom1

```bash
python train.py --config "_custom_config/reduced3_disc.yaml" --data_root "training/by_eye_custom_fov90/01c-r3-ig1_train" --log_interval 200 --log_folder logs_reference_P
```

### 01c reduced4 - input, gauss_custom1

```bash
python train.py --config "_custom_config/reduced4_disc.yaml" --data_root "training/by_eye_custom_fov90/01c-r4-ig1_train" --log_interval 200 --log_folder logs_reference_P
```

### 01c reduced5 - input, gauss_custom1

```bash
python train.py --config "_custom_config/reduced5_disc.yaml" --data_root "training/by_eye_custom_fov90/01c-r5-ig1_train" --log_interval 200 --log_folder logs_reference_P
```

### 01c reduced6 - input, gauss_custom1

```bash
python train.py --config "_custom_config/reduced6_disc.yaml" --data_root "training/by_eye_custom_fov90/01c-r6-ig1_train" --log_interval 200 --log_folder logs_reference_P
```

### 01c reduced7 - input, gauss_custom1

```bash
python train.py --config "_custom_config/reduced7_disc.yaml" --data_root "training/by_eye_custom_fov90/01c-r7-ig1_train" --log_interval 200 --log_folder logs_reference_P
```

### 01c reduced8 - input, gauss_custom1

```bash
python train.py --config "_custom_config/reduced8_disc.yaml" --data_root "training/by_eye_custom_fov90/01c-r8-ig1_train" --log_interval 200 --log_folder logs_reference_P
```

### 01c reduced9 - input, gauss_custom1

```bash
python train.py --config "_custom_config/reduced9_disc.yaml" --data_root "training/by_eye_custom_fov90/01c-r9-ig1_train" --log_interval 200 --log_folder logs_reference_P
```

### 01c reduced - gauss_custom1

```bash
python train.py --config "_custom_config/reduced.yaml" --data_root "training/by_eye_custom_fov90/01c-r-g1_train" --log_interval 200 --log_folder logs_reference_P
```

### 01c reduced - input, gauss_custom2

```bash
python train.py --config "_custom_config/reduced_disc.yaml" --data_root "training/by_eye_custom_fov90/01c-r-ig2_train" --log_interval 200 --log_folder logs_reference_P
```

### 01c reduced - gauss_custom2

```bash
python train.py --config "_custom_config/reduced.yaml" --data_root "training/by_eye_custom_fov90/01c-r-g2_train" --log_interval 200 --log_folder logs_reference_P
```

## bec_fov90_bunny

### 01c reduced - input, gauss_custom1

```bash
python train.py --config "_custom_config/reduced_disc.yaml" --data_root "training/bec_fov90_bunny/01c-r-ig1_train" --log_interval 200 --log_folder logs_reference_P
```

## bec_fov90_golem

### 01c reduced - input, gauss_custom1

```bash
python train.py --config "_custom_config/reduced_disc.yaml" --data_root "training/bec_fov90_golem/01c-r-ig1_train" --log_interval 200 --log_folder logs_reference_P
```