import sys

from PIL import Image
import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
from torchvision import models
from torchvision import transforms
from pytorch_model_summary import summary

from models import GeneratorJ


if __name__ == "__main__":
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        print("usage: summary.py <in_model>")
        print("       summary.py <in_model> <inputs count>")
    else:
        in_model_file = sys.argv[1]
        channels = 3
        if len(sys.argv) == 3:
            channels = 3*int(sys.argv[2])

        device = torch.device("cuda")

        with torch.no_grad():
            in_model = torch.load(in_model_file)
            example = torch.rand(1, channels, 512, 512).to(device)
            print(summary(in_model, example, show_input=True))
 
