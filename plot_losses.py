import sys
import torch
import matplotlib.pyplot as plt
import numpy as np

if __name__ == "__main__":
    if len(sys.argv) < 2 or len(sys.argv) > 4:
        print("usage: plot_losses.py <losses_log>")
        print("       plot_losses.py <losses_log> <skip_first>")
        print("       plot_losses.py <losses_log> <skip_first> <batches_interval>")
    else:
        losses_log_file = sys.argv[1]
        losses_log = torch.load(losses_log_file)

        max_batch_num = losses_log.size()[0] + 1

        losses = losses_log.numpy()
        batches_interval = 1
        skip_first = 0
        if len(sys.argv) >= 3:
            skip_first = int(sys.argv[2])
            losses = losses[skip_first:]
        
        if len(sys.argv) == 4:
            batches_interval = int(sys.argv[3])
            num_intervals = losses.shape[0] / batches_interval
            losses = np.array(np.vsplit(losses,num_intervals)).sum(1) / batches_interval

        batches = np.arange(skip_first + batches_interval, max_batch_num, batches_interval)

        plt.plot(batches, losses[:, 0], label='discriminator')
        plt.plot(batches, losses[:, 1], label='adversarial')
        plt.plot(batches, losses[:, 2], label='image')
        plt.plot(batches, losses[:, 3], label='perception')
        plt.plot(batches, losses[:, 4], label='generator')
        plt.xlabel('batch number')
        plt.ylabel('loss')
        plt.title("Losses")
        plt.legend()
        plt.show()
 
 
