#! /usr/bin/env python

# See README.txt for information and build instructions.

import nndata_pb2
import sys

import argparse
import os
import sys
import time
import re

import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
from torchvision import models

from models import GeneratorJ

class NeuralNetSerializer:
    def __init__(self, model, norm_layer='batch_norm', tanh=True):
        self.model = model
        self.use_bias = model.use_bias
        self.resnet_blocks = model.resnet_blocks
        self.append_smoothers = model.append_smoothers
        self.norm_layer = norm_layer
        self.tanh = tanh
        self.opIndex = 0
        self.neuralNet = nndata_pb2.NeuralNet()

    def start_layer(self):
        self.opIndex += 1
        layer = self.neuralNet.layers.add()
        layer.id = self.opIndex
        return layer


    def serialize_Conv2d(self, conv2d, use_bias):
        layer = self.start_layer()
        s_conv2d = nndata_pb2.Conv2d()
        s_conv2d.in_channels = conv2d.in_channels
        s_conv2d.out_channels = conv2d.out_channels
        s_conv2d.kernel_size_x = conv2d.kernel_size[0]
        s_conv2d.kernel_size_y = conv2d.kernel_size[1]
        s_conv2d.stride_x = conv2d.stride[0]
        s_conv2d.stride_y = conv2d.stride[1]
        s_conv2d.use_bias = use_bias

        params = list(conv2d.parameters())
        # weights
        # size: out_channels, in_channels, kernel_size.x, kernel_size.y
        s_conv2d.weights.extend(torch.flatten(params[0]).tolist())

        # biases
        # size: out_channels
        if use_bias:
            s_conv2d.biases.extend(torch.flatten(params[1]).tolist())
        layer.conv2d.CopyFrom(s_conv2d)

    def serialize_Upsampling(self):
        layer = self.start_layer()
        s_upsampling = nndata_pb2.Upsampling()
        s_upsampling.scale_factor = 2
        layer.upsampling.CopyFrom(s_upsampling)

    def serialize_Tanh(self):
        layer = self.start_layer()
        layer.tanh.CopyFrom(nndata_pb2.Tanh())

    def serialize_ReLU(self):
        layer = self.start_layer()
        layer.reLU.CopyFrom(nndata_pb2.ReLU())

    def serialize_LeakyReLU(self, negative_slope):
        layer = self.start_layer()
        s_leakyReLU = nndata_pb2.LeakyReLU()
        s_leakyReLU.negative_slope = negative_slope
        layer.leakyReLU.CopyFrom(s_leakyReLU)

    def serialize_BatchNorm2d(self, batch_norm2d):
        layer = self.start_layer()
        s_batch_norm2d = nndata_pb2.BatchNorm2d()
        s_batch_norm2d.num_features = batch_norm2d.num_features
        s_batch_norm2d.eps = batch_norm2d.eps
        s_batch_norm2d.momentum = batch_norm2d.momentum
        s_batch_norm2d.affine = batch_norm2d.affine

        s_batch_norm2d.means.extend(torch.flatten(batch_norm2d.running_mean).tolist())
        s_batch_norm2d.vars.extend(torch.flatten(batch_norm2d.running_var).tolist())
        params = list(batch_norm2d.parameters())
        s_batch_norm2d.gammas.extend(torch.flatten(params[0]).tolist())
        s_batch_norm2d.betas.extend(torch.flatten(params[1]).tolist())

        layer.batchNorm2d.CopyFrom(s_batch_norm2d)
        

    def serialize_InstanceNorm2d(self, instance_norm2d):
        layer = self.start_layer()
        s_instance_norm2d = nndata_pb2.InstanceNorm2d()
        s_instance_norm2d.num_features = instance_norm2d.num_features
        s_instance_norm2d.eps = instance_norm2d.eps
        s_instance_norm2d.momentum = instance_norm2d.momentum
        s_instance_norm2d.affine = instance_norm2d.affine

        params = list(instance_norm2d.parameters())
        s_instance_norm2d.gammas.extend(torch.flatten(params[0]).tolist())
        s_instance_norm2d.betas.extend(torch.flatten(params[1]).tolist())

        layer.instance_norm2d.CopyFrom(s_instance_norm2d)

    def serialize_Add(self, idx1, idx2):
        layer = self.start_layer()
        s_add = nndata_pb2.Add()
        s_add.id1 = idx1
        s_add.id2 = idx2
        layer.add.CopyFrom(s_add)
    
    def serialize_Cat2(self, idx1, idx2, dim):
        layer = self.start_layer()
        s_cat2 = nndata_pb2.Cat2()
        s_cat2.id1 = idx1
        s_cat2.id2 = idx2
        s_cat2.dim = dim
        layer.cat2.CopyFrom(s_cat2)

    def serialize_Cat3(self, idx1, idx2, idx3, dim):
        layer = self.start_layer()
        s_cat3 = nndata_pb2.Cat3()
        s_cat3.id1 = idx1
        s_cat3.id2 = idx2
        s_cat3.id3 = idx3
        s_cat3.dim = dim
        layer.cat3.CopyFrom(s_cat3)

    # sequence blocks

    def serialize_norm(self, layer):
        if self.norm_layer != None:
            if self.norm_layer == 'batch_norm':
                self.serialize_BatchNorm2d(layer)
            if self.norm_layer == 'instance_norm':
                self.serialize_InstanceNorm2d(layer)

    def serialize_relu_layer(self, layer):
        self.serialize_Conv2d(layer.conv, self.use_bias)
        self.serialize_norm(layer.normalization)
        self.serialize_LeakyReLU(.2)

    def serialize_resnet_block(self, layer):
        self.serialize_ReLU()
        self.serialize_Conv2d(layer.conv_0, self.use_bias)
        self.serialize_norm(layer.normalization)
        self.serialize_ReLU()
        self.serialize_Conv2d(layer.conv_1, self.use_bias)


    def serialize_upconv_layer_upsample_and_conv(self, layer):
        self.serialize_Upsampling()
        self.serialize_Conv2d(layer[1], False)
        self.serialize_norm(layer[2])
        self.serialize_ReLU()

    def serialize_conv_11(self):
        self.serialize_Conv2d(self.model.conv_11[0], self.use_bias)
        self.serialize_ReLU()

    def serialize_conv_11_a(self):
        if self.append_smoothers:
            self.serialize_Conv2d(self.model.conv_11_a[0], self.use_bias)
            self.serialize_ReLU()
            self.serialize_BatchNorm2d(self.model.conv_11_a[2])
            self.serialize_Conv2d(self.model.conv_11_a[3], self.use_bias)
            self.serialize_ReLU()

    def serialize_conv_12(self):
        if self.tanh:
            self.serialize_Conv2d(self.model.conv_12[0], True)
            self.serialize_Tanh()
        else:
            self.serialize_Conv2d(self.model.conv_12, True)


    # whole net

    def serialize_model(self):
        self.serialize_relu_layer(self.model.conv0)
        conv0_idx = self.opIndex
        self.serialize_relu_layer(self.model.conv1)
        conv1_idx = self.opIndex
        self.serialize_relu_layer(self.model.conv2)
        conv2_idx = self.opIndex
        add_idx = self.opIndex
        for i in range(self.resnet_blocks):
            self.serialize_resnet_block(self.model.resnets[i])
            self.serialize_Add(add_idx, self.opIndex)
            add_idx = self.opIndex
        self.serialize_Cat2(conv2_idx, self.opIndex, 1)
        self.serialize_upconv_layer_upsample_and_conv(self.model.upconv2)
        self.serialize_Cat2(conv1_idx, self.opIndex, 1)
        self.serialize_upconv_layer_upsample_and_conv(self.model.upconv1)
        self.serialize_Cat3(conv0_idx, self.opIndex, 0, 1)
        self.serialize_conv_11()
        self.serialize_conv_11_a()
        self.serialize_conv_12()
        return self.neuralNet


if __name__ == "__main__":
    with torch.no_grad():
        style_model = torch.load("res/to_red.pth")
        serializer = NeuralNetSerializer(style_model)
        neuralNet = serializer.serialize_model()
        with open("testing/to_red.nndata", "wb") as f:
          f.write(neuralNet.SerializeToString())

