# realtime_style_transfer_3d_python_scripts

This repository contains python stcripts for training neural network models and related utilities
used in project [realtime_style_transfer_3d](https://gitlab.com/shetr/realtime_style_transfer_3d).
Big part of the code is copy of [Few-Shot-Patch-Based-Training](https://github.com/OndrejTexler/Few-Shot-Patch-Based-Training) with some changes to it.

## Preparing environment

Install conda packages:

```bash
conda create rst3d
conda install numpy=1.19 pillow=7.2 pyyaml=5.3 scikit-image=0.17 scipy=1.5 tensorflow=1.15 pytorch torchvision torchaudio cudatoolkit=10.2 -c pytorch
conda install -c conda-forge opencv
```

Activate conda environment:

```bash
conda activate rst3d
```