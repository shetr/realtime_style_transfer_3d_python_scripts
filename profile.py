import sys

from PIL import Image
import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
from torchvision import models
from torchvision import transforms
from torch.profiler import profile, record_function, ProfilerActivity

from models import GeneratorJ


if __name__ == "__main__":
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        print("usage: profile.py <in_model>")
        print("       profile.py <in_model> <inputs count>")
    else:
        in_model_file = sys.argv[1]
        channels = 3
        if len(sys.argv) == 3:
            channels = 3*int(sys.argv[2])

        device = torch.device("cuda")

        with torch.no_grad():
            in_model = torch.load(in_model_file)
            example = torch.rand(1, channels, 512, 512).to(device)
            # for memmory add: profile_memory=True
            # for shapes add: record_shapes=True
            with profile(activities=[ProfilerActivity.CPU, ProfilerActivity.CUDA]) as prof:
                with record_function("model_inference"):
                    in_model(example)
            # basic: print(prof.key_averages().table(sort_by="cuda_time_total", row_limit=10))
            # shapes: print(prof.key_averages(group_by_input_shape=True).table(sort_by="cuda_time_total", row_limit=10))
            # memmory: print(prof.key_averages().table(sort_by="self_cpu_memory_usage", row_limit=10))
            # chrome tracing: prof.export_chrome_trace("trace.json")
            print(prof.key_averages().table(sort_by="cuda_time_total", row_limit=10))
 
